# git-it-react

**NOTE:** This is a clone of [git-it-electron](https://github.com/jlord/git-it-electron)
using [React.js](https://react.dev/), and newer versions of
[Electron](https://www.electronjs.org). This project retain the simplicity of
[git-it-electron](https://github.com/jlord/git-it-electron) and tries to
resolve issues of the original project, and also add new features.

Git-it is a desktop (Linux, Mac, and Windows) app that teaches you how to use
[Git](https://git-scm.com/) and [GitHub](https://github.com).

## Differences with [git-it-electron](https://github.com/jlord/git-it-electron)

Visually there is no much differences between the original project, but
at project structure and code level there is the following changes:

- UI is now written in React.js
- Uses TypeScript as main programming language
- Remove included binaries
- Uses new electron version
- Uses i18next for localization
- More content (from [Pro Git 2](https://github.com/progit/progit2))

## Installation

You need the following things to start using this application:

- git (of course)
- text editor (Vim, VSCode, Atom, Notepad++)
- git-it-react (duh! obvious)

Here is a quick guide of how you can get all of the previous dependencies:

### Git

In most Unix-like environments this is an easy step, because you can install
directly using a package manager, for example `brew` in MacOS, `apt` in Debian
based.

But for Windows this isn't that easy, because [git](https://git-scm.com/)
doesn't offer official support for Windows.

But don't worry, here I will try cover the git installation of these three
platforms (Linux, Mac, and Windows).

- **Linux** - From the [Linux download section](https://git-scm.com/download/linux):
  - Debian based: `sudo apt-get install git`
  - Fedora: `sudo dnf install git` or `sudo yum install git`
  - Gentoo: `sudo emerge --ask --verbose dev-vcs/git`
  - ArchLinux: `sudo pacman -S git`
  - OpenSUSE: `sudo zypper install git`
  - Others: `You must known how to do it :)`

- **macOS** - From the [macOS download section](https://git-scm.com/download/mac)
  - Homebrew: `brew install git`
  - MacPorts: `sudo port install git`
  - XCode: See <https://developer.apple.com/xcode/>

- **Windows** - There are two easy ways to get git working on Windows:
  - Github Desktop: See <https://desktop.github.com/>
  - Git for Windows: See <https://github.com/git-for-windows/git> and
  <https://gitforwindows.org/>

NOTE: In the Windows installation you must ensure that the Git binaries are
added to the `PATH` variable.

### Text editor

In this field, there are many options, you must choose the one that fit your needs.

I'll recommend the following text editors based on my experience:

- [VSCode](https://code.visualstudio.com/) (For Windows, macOS and Linux)
- [Neovim](https://neovim.io/) (For Linux, and macOS)

These are good in my opinion but you can choose even
[Notepad++](https://github.com/notepad-plus-plus/notepad-plus-plus).

### Git-it-react

Go to [releases](https://gitlab.com/Devil64-Dev/git-it-react/-/releases)
page and select the one that apply to your platform.

To select the right one you need to verify the following things:

- Architecture: `x64`, or `x32` bits.
- OS: `Linux`, `macOS`, or `Windows`.

In base of that you can choose the right version for your platform, for example
for a 32-bit Windows system, you must select: `git-it-react_-windows_x32.zip`.

Now here is the full steps to get `git-it-react` working on your system:

- Download the latest release file that works on your platform.
- Extract it, after you will see a folder called `git-it-react-{platform}`.
- Enters to that folder and execute the file `git-it-react`.
  - On Windows: double-click on the file, or right click and select Open.
  - On macOS: same as in Windows.
  - On Linux: do as you want, or right-click and execute.

NOTE: `{platform}` is one of the following: `linux`, `macos` and `windows`.

I will work to provide best install solutions for all platforms.

## Tips for get started

## Copyright

Copyright (C) 2023  Luis Fernando C.A <devil64dev@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

This project also contains some ideas and resources from the
[git-it-electron](https://github.com/jlord/git-it-electron) project
located at <https://github.com/jlord/git-it-electron>, which is
copyrighted by Jessica Lord and other contributors, and licensed under
The 2-Clause BSD License.
